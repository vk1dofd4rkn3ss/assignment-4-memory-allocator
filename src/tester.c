#include "tester.h"

void common_allocate() {
    printf("Test 1: normal successful memory allocation\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc = _malloc(REGION_MIN_SIZE);

    if (!malloc){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    _free(malloc);
    munmap(HEAP_START,4096);
    printf("Success.\n");
}
void free_one_block() {
    printf("Test 2: freeing one block from several allocated ones\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc1 = _malloc(REGION_MIN_SIZE);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    void *malloc3 = _malloc(REGION_MIN_SIZE);
    if(!malloc1 || !malloc2 || !malloc3){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout, heap);
    _free(malloc3);
    debug_heap(stdout, heap);
    _free(malloc2);
    _free(malloc1);
    munmap(HEAP_START, 4096);
    printf("Success.\n");
}
void free_two_blocks() {
    printf("Test 3: freeing two blocks from several allocated ones.\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc1 = _malloc(REGION_MIN_SIZE);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    void *malloc3 = _malloc(REGION_MIN_SIZE);
    void *malloc4 = _malloc(REGION_MIN_SIZE);
    void *malloc5 = _malloc(REGION_MIN_SIZE);
    if(!malloc1 || !malloc2 || !malloc3 || !malloc4 || !malloc5){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout,heap);
    _free(malloc1);
    _free(malloc2);
    debug_heap(stdout,heap);
    _free(malloc3);
    _free(malloc4);
    _free(malloc5);
    munmap(HEAP_START, 4096);
    printf("Success.\n");
}
void new_region_extends_old() {
    printf("Test 4: the memory has run out, the new memory region expands the old one\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        fprintf(stderr,"%s","error heap");
        return;
    }
    void *malloc = _malloc((REGION_MIN_SIZE + 20));
    if(!malloc){
        fprintf(stderr,"%s","error malloc");
        return;
    }
    debug_heap(stdout, heap);
    _free(malloc);
    munmap(HEAP_START, 16384);
    printf("Success.\n");
}
void new_region_does_not_extend_old() {
    printf("Test 5: the memory has ended, the old memory region cannot be expanded due to another allocated address range, the new region\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        printf("error heap\n");
        return;
    }
    struct block_header *bh = (struct block_header *) heap;
    void *malloc1 = _malloc(bh->capacity.bytes);
    debug_heap(stdout, heap);
    void *map = mmap(bh->contents + bh->capacity.bytes, REGION_MIN_SIZE*2, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    void *malloc2 = _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    _free(malloc1);
    _free(malloc2);
    munmap(map, size_from_capacity((block_capacity) {.bytes=5000}).bytes);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=REGION_MIN_SIZE}).bytes);
    printf("Success.\n");
}
void test() {
    common_allocate();
    free_one_block();
    free_two_blocks();
    new_region_extends_old();
    new_region_does_not_extend_old();
}
